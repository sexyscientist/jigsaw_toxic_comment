from os import path, environ, remove, mkdir
from glob import glob
import re

import requests
from requests.packages.urllib3.exceptions import InsecureRequestWarning

import json
import yaml

import datetime as dt

from time import sleep

from bs4 import BeautifulSoup

import pandas as pd

import argparse

from itertools import repeat, chain

DATE_FORMAT = '%Y%m%d'
CSE_API_URL = "https://www.googleapis.com/customsearch/v1"


def parse_yaml(p):
    if path.exists(p):
        with open(p, 'r') as f:
            params = yaml.safe_load(f)
            return params
    return None

def recursive_get(key, origins, default=''):
    if len(origins) > 0:
        return origins[0].get(key, recursive_get(key, origins[1:], default)) or recursive_get(key, origins[1:], default) or default
    return default

def request(url, method="GET", params={}, default=None):
    r = default
    for trials in range(1, 5):
        try:
            r = requests.request(method, url, params=params, verify=False)
            if r.status_code == requests.codes.ok:
                break
        except requests.exceptions.ConnectionError as ex:
            sleep(0.5*trials)
    
    if hasattr(r, 'status_code'):
        return r if r.status_code == requests.codes.ok else default
    
    return default

def ordered_period(initial_date, period='0D'):
    final_date = initial_date + pd.to_timedelta(period)
    return tuple(sorted([initial_date, final_date]))

def mcd(a, b):    
    a, b = max(a, b), min(a, b)
    mod = a % b
    return mcd(b, mod) if mod else b

def mcm(a, b):
    return (a * b) // mcd(a, b)

class NewsArticleScraper:

    def __init__(self, edges, meta_edge_dict):
        self.edges = edges
        self.meta_edge_dict = meta_edge_dict

    def preprocess(self, string):
        return re.sub(r'([^\n\t\r]*)([\n\t\r]*)([^\n\t\r]*)', r'\1 \3', string)

    def __call__(self, day, period='0D'):

        initial_date, final_date = ordered_period(day, period)

        initial_date = initial_date.strftime(DATE_FORMAT)
        final_date = final_date.strftime(DATE_FORMAT)

        parameters.update({
            'sort': "date:r:%s:%s" % (initial_date, final_date)
        })

        """ Process CSE query """
        cntr = 0
        while cntr < 5:
            sleep(3 * cntr)
            page = request(CSE_API_URL, params=parameters)
            if page is not None:
                break
            cntr += 1

        if page is None:
            return None

        response_data = json.loads(page.text)

        if 'items' not in response_data:
            return []

        result = []
        for item in response_data['items']:
            result += [{}]

            result[-1]['url'] = item['link']

            """ Scrap all possible edges via BeautifulSoup based on NewsArticle microformat """
            response = request(item['link'])
            if response is not None:
                html = response.content
                soup = BeautifulSoup(html, "html.parser")

                [s.extract() for s in soup('script')]
                [s.extract() for s in soup('style')]
                # [s.extract() for s in soup('meta')]

            for edge in self.edges:
                if edge not in result[-1]:
                    if response is not None:
                        found = soup.select('[itemprop="' + edge + '"]')
                        if len(found) > 0:
                            result[-1][edge] = self.preprocess(found[0].get_text())
                        else:
                            result[-1][edge] = None
                    else:
                        result[-1][edge] = None

            if 'pagemap' in item:

                """ Try and fill with pagemap's schema microformat-like named tags """
                for edge in [edge for edge, val in result[-1].items() if not val]:
                    if (edge.lower() in item['pagemap']['newsarticle'][0]) \
                            if 'newsarticle' in item['pagemap'] \
                            else False:
                        result[-1][edge] = self.preprocess(item['pagemap']['newsarticle'][0][edge.lower()])
                    else:
                        if 'metatags' in item['pagemap']:
                            if edge.lower() in item['pagemap']['metatags'][0]:
                                result[-1][edge] = self.preprocess(item['pagemap']['metatags'][0][edge.lower()])

                """ Try and fill with known pagemap's metatags """
                for edge in [edge for edge, val in result[-1].items() if not val and edge in self.meta_edge_dict]:
                    for tag in self.meta_edge_dict[edge]:
                        if 'metatags' in item['pagemap']:
                            if tag in item['pagemap']['metatags'][0]:
                                result[-1][edge] = self.preprocess(item['pagemap']['metatags'][0][tag])
                                break

        return result


if __name__ == '__main__':

    parser = argparse.ArgumentParser(description="Google Search Fetch script")
    parser.add_argument('--query-ids', '-i', nargs='+', type=int, default=['*'],
        help='IDs for the query(ies) to be fetched (from `config.yml` file)')
    parser.add_argument('--sleep-time', '-s', type=str, default='5s',
        help='Minimum delay time between queries')
    parser.add_argument('--key-ids', '-k', nargs='+', type=int, default=['*'],
        help='CSE API keys to be used for parsing (associated to different Google Cloud accounts)')
    parser.add_argument('--use-non-recommended-keys', action='store_true', default=False,
        help='Use also keys marked as non recommended.')

    args = parser.parse_args()

    qids = args.query_ids
    s_time = args.sleep_time
    kids = args.key_ids
    use_non_recommended = args.use_non_recommended_keys

    """Directory handling."""
    dirname = path.dirname(path.realpath(__file__))
    datadir = path.join(dirname, "input_data")

    if not path.exists(datadir):
        mkdir(datadir)
    
    api_error = False
    api_error_cnt = 0

    while True:

        SECRET = parse_yaml('secret.yml')
        CONFIG = parse_yaml('config.yml')

        configinputs = recursive_get('inputs', [CONFIG, environ], [])
        cse_keys = recursive_get('api_keys', [SECRET.get('google_search_api', dict()), environ], [])

        cse_keys = [
            k['key'] for k in cse_keys if (use_non_recommended or k['recommended']) and ('*' in kids or k['id'] in kids)
        ]
        configinputs = [c for c in configinputs if ('*' in qids or c['id'] in qids)]

        mcm_len = mcm(len(cse_keys), len(configinputs))
        chain_keys = chain(cse_keys*int(mcm_len/len(cse_keys)))
        chain_configinputs = chain(configinputs*int(mcm_len/len(configinputs)))

        api_error_cnt = 0 if not api_error else api_error_cnt + 1
        api_error = False

        if api_error_cnt > (len(cse_keys) *2):
            print("[ERROR] API call limits seem to be exhausted for today. Maybe try again next day! :)")
            exit(0)

        for configinput, key in zip(chain_configinputs, chain_keys):

            """Setup Query Parameters"""
            parameters = {
                "q": configinput['query'],
                "cx": configinput['cx'],
                "key": key,
                "lr": configinput['language']
            }

            """Instantiate scraper"""
            scraper = NewsArticleScraper(
                edges=["articleBody", "author", "creator", "dateCreated",
                    "datePublished", "dateModified", "keywords", 'title'],
                meta_edge_dict={
                    "dateModified": ["article:modified_time", "date"],
                    "datePublished": ["article:published_time", "dc.date.issued"],
                    "keywords": ["article:tag"],
                    "title": ["og:title", "twitter:title"]
                }
            )
            
            requests.packages.urllib3.disable_warnings(InsecureRequestWarning)

            start_date = dt.datetime.strptime(configinput['start_date'], DATE_FORMAT).date()
            query_id = "q%d" % (configinput['id'],)
            query_period = configinput['timedelta']
            query_period_timedelta = pd.to_timedelta(query_period)

            today = dt.datetime.today().date()
            today_results = pd.DataFrame()
            tic = dt.datetime.now()

            """Remove files older than start_date"""
            dates = []
            for each in [f for f in glob(path.join(datadir, query_id + "_*.csv"))]:
                date_string = path.splitext(each)[0].split('_')[-1]
                if re.match(r'^[0-9]{8}$', date_string) is not None:
                    date = dt.datetime.strptime(date_string, DATE_FORMAT).date()
                    if date < start_date:
                        remove(each)
                        continue
            
                    dates += [date]
            
            """Maybe add non-existent file"""
            for date in [
                start_date + d*query_period_timedelta for d in range(int((today - start_date)/query_period_timedelta))]:
                if date not in dates and not api_error:
            
                    # One call per input every s_time at most
                    toc = dt.datetime.now()
                    if toc - tic < pd.to_timedelta(s_time):
                        sleep((pd.to_timedelta(s_time)-(toc-tic)).seconds)
                    tic = dt.datetime.now()
            
                    filepath = path.join(datadir, query_id + '_' + date.strftime(DATE_FORMAT) + '.csv')
            
                    scraped = scraper(date, period=query_period)
                    api_error = scraped is None
                    if not api_error:
                        pd.DataFrame(scraped).to_csv(filepath, sep='\t', encoding='utf-8')
                    
                    break
