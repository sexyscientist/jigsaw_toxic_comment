from os import path, environ, remove, mkdir
from glob import glob
import re

from itertools import repeat, zip_longest
from functools import reduce
from operator import add, itemgetter

import yaml

import datetime as dt

import requests
from requests.packages.urllib3.exceptions import InsecureRequestWarning
from bs4 import BeautifulSoup

import pandas as pd
import numpy as np

import argparse
import click
from time import sleep

import sys
from colorama import Fore, Back, Style, init, AnsiToWin32

import joblib

init(wrap=False)
stream = AnsiToWin32(sys.stderr).stream
            
requests.packages.urllib3.disable_warnings(InsecureRequestWarning)

DATE_FORMAT = '%Y%m%d'
QUOTE_REGEXES = list(map(re.compile, [
    r'«+([^»]*?)»+',
    r'“+([^”]*?)”+',
    r'‘+([^’]*?)’+',
    r'"([^"]*?)"',
    r'""(.*?)""',
    r'"""(.*?)"""',
    r'\'\'(.*?)\'\'',
    r'\'\'\'(.*?)\'\'\''
]))
QUOTE_DF_SCHEMA = ['start_date', 'end_date', 'input_idx', 'input_url', 'quote', 'manual_label', 'auto_label']
PARSED_DATES_FILE = 'parsed_dates.txt'

def parse_yaml(p):
    if path.exists(p):
        with open(p, 'r') as f:
            params = yaml.safe_load(f)
            return params
    return None

def recursive_get(key, origins, default=''):
    if len(origins) > 0:
        return origins[0].get(key, recursive_get(key, origins[1:], default)) or recursive_get(key, origins[1:], default) or default
    return default

def grouper(iterable, n, fillvalue=None):
    "Collect data into fixed-length chunks or blocks"
    # grouper('ABCDEFG', 3, 'x') --> ABC DEF Gxx
    args = [iter(iterable)] * n
    return zip_longest(fillvalue=fillvalue, *args)

def fixed_width_print(text, max_width=200, padding=2, end="\n"):
    print(
        "\n".join(map(lambda x: ''.join(x).center(max_width, ' '), grouper(text, max_width-padding*2, ' '))),
        file=stream,
        end=end
    )

def click_prompt(prompt_msg, yes_options=[chr(13), 'y', 'Y'], header='', clr=True, nl=False):
    if clr:
        click.clear()
    
    if header is not None and len(header) > 0:
        fixed_width_print(header, end="\n\n")
    
    click.echo(prompt_msg, nl=nl)

    return click.getchar() in yes_options

def manual_labeler(quote):
    if click_prompt("Keep this quote? ([Y]/n): ", header=quote):
        return quote, click_prompt("Label as Toxic? ([Y]/n): ", header=quote)    
    return quote, None

def show_quotes(text, quote_spans):

    quote_start = Fore.GREEN + Back.WHITE
    quote_end = Style.RESET_ALL

    q_s = list(map(itemgetter(0), quote_spans))
    q_e = list(map(itemgetter(1), quote_spans))
    
    _ = click_prompt("Press any key to continue...", header="".join([
        (quote_start + c if i in q_s else (quote_end + c if i in q_e else c)) for i, c in enumerate(text)
    ]))

def request(url, method="GET", params={}, default=None):
    r = default
    for trials in range(1, 5):
        try:
            r = requests.request(method, url, params=params, verify=False)
            if r.status_code == requests.codes.ok:
                break
        except requests.exceptions.ConnectionError as ex:
            sleep(0.5*trials)
    
    if hasattr(r, 'status_code'):
        return r if r.status_code == requests.codes.ok else default
    
    return default

def is_article(text):
    return len(articleBody) > 1 and any(map(str.isalpha, articleBody[:50]))

def check_parsed_article(qid, iid, date):
    if not path.exists(PARSED_DATES_FILE):
        return False
    
    with open(PARSED_DATES_FILE, 'r') as fp:
        parsed_inputs_n_dates = [line[:-1].split('_') for line in fp.readlines()]
        parsed_inputs_n_dates = list(map(
            lambda x: (x[0], dt.datetime.strptime(x[1], DATE_FORMAT).date(), x[2]),
            parsed_inputs_n_dates
        ))
    
    if ("q%d" % (qid,), date, str(iid)) in parsed_inputs_n_dates:
        return True
    
    return False

def mark_parsed_article(qid, iid, date):
    with open(PARSED_DATES_FILE, 'a') as fp:
        fp.write("q%d" % (qid,) + "_" + date.strftime(DATE_FORMAT) + "_" + str(iid) + "\n")

if __name__ == '__main__':

    parser = argparse.ArgumentParser(description="Quote retrieval script (Follows Google Search Fetch script)")
    parser.add_argument('--query-ids', '-i', nargs='+', type=int, default=['*'],
        help='IDs for the query(ies) to be fetched (from `config.yml` file)')
    parser.add_argument('--manual-annotation', '-m', action='store_true', default=False,
        help='Interactively ask the user to filter and label quotes.')
    parser.add_argument('--automatic-annotation', '-a', action='store_true', default=False,
        help='Include automatic labeling (takes labeling model from ./labeling_model/ folder).')
    parser.add_argument('--rewrite-labels', '-r', action='store_true', default=False,
        help='Whether to rewrite labels (asks first for manual if selected).')
    parser.add_argument('--check-quote-retrieval', '-c', action='store_true', default=False,
        help='Whether to rewrite labels (asks first for manual if selected).')
    parser.add_argument('--try-rescraping-fallback', action='store_true', default=False,
        help='Whether to try to scrape heuristically those results for which we don\'t have an ArticleBody.')
    parser.add_argument('--sleep-time', '-s', type=str, default='5s',
        help='Minimum delay time between queries')

    args = parser.parse_args()

    qids = args.query_ids
    m_annot = args.manual_annotation
    a_annot = args.automatic_annotation
    rw_lbls = args.rewrite_labels
    chk_qtr = args.check_quote_retrieval
    rescrap = args.try_rescraping_fallback
    s_time = args.sleep_time

    """Directory handling."""
    dirname = path.dirname(path.realpath(__file__))
    datadir = path.join(dirname, "input_data")
    quotedir = path.join(dirname, "quote_data")
    modeldir = path.join(dirname, "labeling_model")

    CONFIG = parse_yaml('config.yml')
    configinputs = recursive_get('inputs', [CONFIG, environ], [])
    configinputs = [c for c in configinputs if ('*' in qids or c['id'] in qids)]
    qids = list(set([c['id'] for c in configinputs if 'id' in c]))

    if not path.exists(quotedir):
        mkdir(quotedir)
    
    tic = dt.datetime.now()
    
    q_files_left = [True]*len(qids)
    while any(q_files_left):

        q_files = dict([(qid, [fp for fp in glob(path.join(datadir, 'q%d_*.csv' % (qid,)))]) for qid in qids])

        for qidx, qid in enumerate(qids):

            quote_file_path = path.join(quotedir, "q%d_quotes.csv" % (qid,))
            q_timedelta = [c['timedelta'] for c in configinputs if c['id'] == qid][0]

            if path.exists(quote_file_path):
                quote_df = pd.read_csv(quote_file_path, sep='\t', parse_dates=[0, 1], header=0)
                if len(quote_df):
                    for col in [0, 1]:
                        quote_df[QUOTE_DF_SCHEMA[col]] = quote_df[QUOTE_DF_SCHEMA[col]].dt.date
            else:
                quote_df = pd.DataFrame(columns=QUOTE_DF_SCHEMA)
            
            annotated_one = False
            q_file = ''
            for q_file in q_files[qid]:

                q_start_date = dt.datetime.strptime(
                    re.sub(r'q\d+_(\d{8})\.csv', r'\1', q_file.split(path.sep)[-1]),
                    DATE_FORMAT
                ).date()
                q_end_date = q_start_date + pd.to_timedelta(q_timedelta)

                input_df = pd.read_csv(q_file, sep='\t', header=0)
                
                for i, url, articleBody, author, creator, dateCreated, datePublished, dateModified, keywords, title in \
                    input_df.itertuples(index=False):

                    try:
                        collision_mask = (quote_df.start_date == q_start_date) & \
                            (quote_df.end_date == q_end_date) & \
                            (quote_df.input_idx == i)
                    except AttributeError as e:
                        print(quote_df)
                        raise e

                    if not rw_lbls and \
                        (len(quote_df[collision_mask]) > 0 or check_parsed_article(qid, i, q_start_date)):
                        continue
                    
                    quote_df = quote_df[~collision_mask]

                    if pd.isnull(articleBody) or len(articleBody) == 0:
                        if not rescrap:
                            continue

                        print("[INFO] Processing GET request...")
                        
                        toc = dt.datetime.now()
                        if toc - tic < pd.to_timedelta(s_time):
                            sleep((pd.to_timedelta(s_time)-(toc-tic)).seconds)
                        tic = dt.datetime.now()

                        response = request(url)
                        if response is not None:
                            html = response.content
                            soup = BeautifulSoup(html, "html.parser")

                            [s.extract() for s in soup('script')]
                            [s.extract() for s in soup('style')]
                            [s.extract() for s in soup('meta')]

                            articleBody = re.sub(r'([^\n\t\r]*)([\n\t\r]*)([^\n\t\r]*)', r'\1 \3', soup.get_text())
                            articleBody = re.sub(r' +', ' ', articleBody)

                            if is_article(articleBody):
                                input_df.iloc[i]['articleBody'] = articleBody

                    if pd.isnull(articleBody) or not is_article(articleBody):
                        mark_parsed_article(qid, i, q_start_date)
                        continue

                    articleBody = re.sub(r' +', ' ', articleBody)

                    if chk_qtr:
                        quote_spans = reduce(
                            add ,
                            map(
                                lambda x: [[sp for sp in qt.span() if sp != (-1, -1)] for qt in x.finditer(articleBody)], QUOTE_REGEXES
                            ),
                            []
                        )
                        show_quotes(articleBody, quote_spans)

                    article_quotes = reduce(add, map(lambda x: x.findall(articleBody), QUOTE_REGEXES), [])

                    if m_annot:
                        annotated_quotes = list(filter(
                            lambda x: x[1] is not None,
                            list(map(manual_labeler, article_quotes))
                        ))
                    else:
                        annotated_quotes = list(zip(article_quotes, repeat(np.nan)))
                    
                    if a_annot:
                        mdl = joblib.load([p for p in glob(path.join(modeldir, '*.joblib'))][0])
                        classified_quotes = list(map(mdl.predict, list(map(itemgetter(0), annotated_quotes))))
                        if not m_annot:
                            classified_quotes = classified_quotes.filter(~np.isnan)
                    else:
                        classified_quotes = [np.nan] * len(annotated_quotes)

                    to_append = pd.DataFrame(
                        list(zip(
                            repeat(q_start_date),
                            repeat(q_end_date),
                            repeat(i),
                            repeat(url),
                            map(itemgetter(0), annotated_quotes),
                            map(itemgetter(1), annotated_quotes),
                            classified_quotes
                        )),
                        columns=QUOTE_DF_SCHEMA
                    )
                    
                    quote_df = quote_df.append(
                        to_append,
                        ignore_index=True,
                        sort=['start_date', 'end_date', 'input_idx']
                    )

                    annotated_one = True
                    mark_parsed_article(qid, i, q_start_date)

                if annotated_one:
                    break
            
            quote_df.to_csv(quote_file_path, columns=QUOTE_DF_SCHEMA, sep='\t', encoding='utf-8', index=False)
            
            q_files_left[qidx] = (q_file != q_files[qid][-1])
    
    print("[INFO] Finnished annotating all quotes! :)")
