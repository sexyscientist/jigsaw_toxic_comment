# Fine-tunnig BERT on Multilingual Binary Classification
---

├── bert-base-multilingual-uncased      
│   ├── config.json      
│   ├── pytorch_model.bin      
│   └── vocab.txt      
├── data      
│   ├── jigsaw-toxic-comment-train.csv      
│   ├── jigsaw-unintended-bias-train.csv      
│   ├── test.csv      
│   └── validation.csv 
├── config.py      
├── dataset.py      
├── docker.sh      
├── main.py      
├── model.py      
└── train.py      
└── utils.py       