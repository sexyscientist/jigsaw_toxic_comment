
import transformers

TPUs = False
MAX_LEN = 128
TRAIN_BATCH_SIZE = 64
VALID_BATCH_SIZE = 64

LR = 3e-5
EPOCHS = 5 # 10

# Choose one & comment the other
# MODEL = 'bert'
MODEL = 'distil-bert'

MODEL_PATH = {
    'bert': 'checkpoints/bert_model.bin', 
    'distil-bert':'checkpoints/distil_bert_model.bin'
}

BERT_PATH = {
    'bert': 'bert-base-multilingual-uncased', 
    'distil-bert':'distilbert-base-multilingual-cased'
}

TOKENIZER = {
    'bert': transformers.BertTokenizer.from_pretrained(
        BERT_PATH['bert'],
        do_lower_case=True
    ),
    'distil-bert': transformers.DistilBertTokenizer.from_pretrained(
        BERT_PATH['distil-bert']
    )
}