

import os
import sys
import time
import json
import yaml
import torch
import pandas as pd
from os.path import join as JP
from sklearn.utils import resample
from collections import OrderedDict
from beautifultable import BeautifulTable as BT


# =============================================================================
# General Utils
# ============================================================================= 

def print_current_config(CUDA, N_GPU, DEVICE, WORKERS):
    # CONFIG 
    MULTI_GPU = True if N_GPU > 1 else False
    # WORKERS = 1
    DEVICE_NAME = torch.cuda.get_device_name(0) if DEVICE == 'cuda' else 'CPUs'

    table = BT()
    table.append_row(['Python', sys.version[:5]])
    table.append_row(['PyTorch', torch.__version__])
    table.append_row(['GPUs', str(N_GPU)])
    table.append_row(['Cores', str(WORKERS)])
    table.append_row(['Device', str(DEVICE_NAME)])
    print('Environment Settings')
    print(table)


def upsample_class_imbalance(df, col, pct=1):
    """
    Rebalance an imbalace dataset
    Args:
        - df: Original dataframe
        - col: Column of the imbalanced class
        - pct: Percentage of balance to reach (default = 1)
    """
    err = 'Percentage should be in range [0,1]'
    assert pct > 0 and pct <= 1, err

    # Split by class
    df_majority = df[df[col] == 0]
    df_minority = df[df[col] == 1]

    # Upsample minority class
    df_minority_upsampled = resample(
        df_minority, 
        replace=True,                               # sample with replacement
        n_samples=int(df_majority.shape[0]*pct)     # to match majority class
    )

    # Combine majority class with upsampled minority class
    df_upsampled = pd.concat([df_majority, df_minority_upsampled])
    return df_upsampled


def count_parameters(model, count_frozen=False):
    ''' Count the parameters of a model '''
    if count_frozen:
        print('Counting all parameters')
        return sum(p.numel() for p in model.parameters())
    else:
        print('Counting non-frozen parameters only')
        return sum(p.numel() for p in model.parameters() if p.requires_grad)


class Results(object):
    
    def __init__(self, name=None):
        self.name = name                
        self.time = list()
        self.epoch = list()
        self.train_loss = list()
        self.train_accy = list()
        self.valid_loss = list()
        self.valid_accy = list()
        self.auc_score = list()